package com.karolapp.ideaapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Rates {
    @SerializedName("asset_id_base")
    private String asset_id_base;

    @SerializedName("rates")
  private List<Cryptocurrency> cryptocurrencyList;

    public List<Cryptocurrency> getCryptocurrencyList() {
        return cryptocurrencyList;
    }

    public void setCryptocurrencyList(List<Cryptocurrency> cryptocurrencyList) {
        this.cryptocurrencyList = cryptocurrencyList;
    }

    public String getAsset_id_base() {
        return asset_id_base;
    }

    public void setAsset_id_base(String asset_id_base) {
        this.asset_id_base = asset_id_base;
    }

    public Rates(String asset_id_base, List<Cryptocurrency> cryptocurrencyList) {
        this.asset_id_base = asset_id_base;
        this.cryptocurrencyList = cryptocurrencyList;
    }
}
