package com.karolapp.ideaapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cryptocurrency  {
    @SerializedName("asset_id_quote")
    @Expose
    private  String name;
    @SerializedName("time")
    @Expose
    private String time;

    @Expose
    @SerializedName("rate")
    private String rate;

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cryptocurrency(String time, String rate, String name) {
        this.time = time;
        this.rate = rate;
        this.name = name;
    }
}
