package com.karolapp.ideaapp.dashboardFragments;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karolapp.ideaapp.R;

import androidx.fragment.app.Fragment;


public class DetailsCurrencyFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    public DetailsCurrencyFragment() {
        // Required empty public constructor
    }


    public static DetailsCurrencyFragment newInstance(String param1, String param2) {
        DetailsCurrencyFragment fragment = new DetailsCurrencyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details_currency, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
