package com.karolapp.ideaapp.dashboardFragments;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.karolapp.ideaapp.R;
import com.karolapp.ideaapp.adapter.RecyclerViewAdapter;
import com.karolapp.ideaapp.api.ApiClient;
import com.karolapp.ideaapp.api.ApiService;
import com.karolapp.ideaapp.databinding.FragmentHomeBinding;
import com.karolapp.ideaapp.holder.CurrencyHolder;
import com.karolapp.ideaapp.model.Cryptocurrency;
import com.karolapp.ideaapp.model.Rates;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class HomeFragment extends Fragment implements CurrencyHolder.OnNoteListener {

    private ApiService service;
    private RecyclerView recyclerView;
    private FragmentHomeBinding fragmentHomeBinding;
    private static final String TAG = "HomeFragment";
    private NavController navController;
    private OnFragmentInteractionListener mListener;
    RecyclerViewAdapter viewAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        service = ApiClient.getClient().create(ApiService.class);

        recyclerView = fragmentHomeBinding.contentRecycler.recyclerView;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        fragmentHomeBinding.setHomeFragemnt(this);

        navController = Navigation.findNavController(getActivity(), R.id.app_bar);
        getData();
        View view = fragmentHomeBinding.getRoot();


        return view;
    }


    private ArrayList<Cryptocurrency> getData() {
        /// dane z giełdy
        final ArrayList<Cryptocurrency> list = new ArrayList<>();
        Log.i(TAG, "onResponse: AFTER");

        service.getCryptocurrency()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Rates>() {
                    @Override
                    public void accept(Rates rates) throws Exception {
                        displayCurrency(rates);
                    }
                });

        return list;

    }

    private void displayCurrency(Rates rates) {
        recyclerView.setAdapter(new RecyclerViewAdapter(getContext(), rates.getCryptocurrencyList(), this));
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onNoteClick(int position, View view) {
        Log.d("click", "onNoteClick: ");
        navController.navigate(R.id.action_homeFragment_to_detailsCurrencyFragment);

    }

    @Override
    public void onStop() {
        //  compositeDisposable.clear();
        super.onStop();
    }
}
