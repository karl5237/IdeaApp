package com.karolapp.ideaapp;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class User extends BaseObservable {

    private String email;
    private String password;
    private static final String TAG = "";

    //
//    public User(String email, String password) {
//        this.email = email;
//        this.password = password;
//    }
    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}