package com.karolapp.ideaapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karolapp.ideaapp.databinding.CryptocurrencyDataBinding;
import com.karolapp.ideaapp.holder.CurrencyHolder;
import com.karolapp.ideaapp.model.Cryptocurrency;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<CurrencyHolder> {
    private Context mContext;
    private ArrayList<Cryptocurrency> mArrayList;
    private LayoutInflater layoutInflater;
    private CurrencyHolder.OnNoteListener onNoteListener;
    private List<Cryptocurrency> currency ;

    public RecyclerViewAdapter(Context mContext, List<Cryptocurrency> mList, CurrencyHolder.OnNoteListener onNoteListener) {
        this.mContext = mContext;
        this.currency = mList;
        this.onNoteListener = onNoteListener;
    }

    @NonNull
    @Override
    public CurrencyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        CryptocurrencyDataBinding cryptocurrencyDataBinding = CryptocurrencyDataBinding.inflate(layoutInflater, parent, false);
        return new CurrencyHolder(cryptocurrencyDataBinding, onNoteListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyHolder holder, int position) {
        Cryptocurrency cryptocurrency = currency.get(position);
        holder.bind(cryptocurrency);
    }

    @Override
    public int getItemCount() {
        return currency.size();
    }

    public void setCurrency(List<Cryptocurrency> currency) {
        this.currency = currency;
    }

    public List<Cryptocurrency> getCurrency() {
        return currency;
    }

    public void updateCurrency(Cryptocurrency cryptocurrency) {
        currency.set(currency.indexOf(cryptocurrency), cryptocurrency);
        notifyItemChanged(currency.indexOf(cryptocurrency));
    }
}
