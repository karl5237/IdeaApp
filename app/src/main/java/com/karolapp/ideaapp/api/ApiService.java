package com.karolapp.ideaapp.api;

import com.karolapp.ideaapp.model.Rates;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

//@path - zmienna do @get({zmienna})
public interface ApiService {
    static String apiKey2 = "28fd0d97-08f7-434c-8b48-9ecafc341750";
    static String apiKey3 = "2917ecad3063f4fb45021fe275adcaededc4223d492a5d35ce6874082265173c";
    static String apiKey = "E23C7AC4-72E4-4CCD-9FE5-B5BDCD99449B";
    @Headers({

            "X-CoinAPI-Key:"+ apiKey
    })
    @GET("exchangerate/USD/")
    Observable<Rates> getCryptocurrency();

}
