package com.karolapp.ideaapp.holder;

import android.view.View;

import com.karolapp.ideaapp.databinding.CryptocurrencyDataBinding;
import com.karolapp.ideaapp.model.Cryptocurrency;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CurrencyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private CryptocurrencyDataBinding cryptocurrencyDataBinding;
    private OnNoteListener onNoteListener;

    public CurrencyHolder(@NonNull CryptocurrencyDataBinding cryptocurrencyDataBinding, OnNoteListener onNoteListener) {
        super(cryptocurrencyDataBinding.getRoot());
        this.cryptocurrencyDataBinding = cryptocurrencyDataBinding;

        this.onNoteListener = onNoteListener;
        cryptocurrencyDataBinding.getRoot().setOnClickListener(this);
    }


    public void bind(Cryptocurrency cryptocurrency) {
        this.cryptocurrencyDataBinding.setCryptocurrency(cryptocurrency);
    }

    public CryptocurrencyDataBinding getCryptocurrencyDataBinding() {
        return cryptocurrencyDataBinding;
    }

    @Override
    public void onClick(View view) {
        onNoteListener.onNoteClick(getAdapterPosition(),view);
    }

    public interface OnNoteListener {
        void onNoteClick(int position, View view);
    }
}
