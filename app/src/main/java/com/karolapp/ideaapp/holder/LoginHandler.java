package com.karolapp.ideaapp.holder;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.karolapp.ideaapp.R;
import com.karolapp.ideaapp.RegisterFragment;
import com.karolapp.ideaapp.User;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class LoginHandler {
    private User user;
    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";
    private static final String TAG = "LoginHandler";
    private FirebaseAuth mAuth;

    public LoginHandler(User user, FirebaseAuth mAuth) {
        this.user = user;
        this.mAuth = mAuth;
    }

    public void buttonLoginClick(View view) {
        if (isInputDataValid()) {
          //  singIn(view);
            Toast.makeText(view.getContext(), successMessage, Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(view.getContext(), errorMessage, Toast.LENGTH_SHORT).show();

    }
    public void goToRegistryScreen(View view){
        RegisterFragment registerFragment = new RegisterFragment();
        FragmentManager fragmentManager =((FragmentActivity) view.getContext()).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main, registerFragment, "Ulubione");
        fragmentTransaction.commit();
    }




    public boolean isInputDataValid() {

        return !TextUtils.isEmpty(user.getEmail()) && Patterns.EMAIL_ADDRESS.matcher(user.getEmail()).matches() && user.getPassword().length() > 5;
    }
}
