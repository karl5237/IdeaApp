# IdeaApp

Aplikacja do sprawdzania kursu kryptowalut oraz danych historycznych z API https://www.coinapi.io/

# Done 
  - Rejstracja przy użyciu firebase
  - Logowanie przy użyciu firebase
  - Automatyczne przejście użytkownika zalogowanego
  - Pobranie aktualnego kursu w dolarach kryptowalut do głównego ekranu (Retrofit)
  - Widok walut przewijalny(RecyclerView)

# Doing
  - Szczegóły waluty
  - Użycie RxJava do asynchronicznego pobrania wartości w tle
  - Pobranie danych historcznych(pobieranie przy użyciu flatMap - RxJava)
# To Do

- Obserwacja ulubionych
- Alarmy na ustalone wartości kursu kryptowaluty
- przelicznik walut z aktualną wartością
- porgnozy
- wykres danych historycznych
- konto użytkownika
- śledzenie potencajlnego zysku na podstawie danych historycznych
- dodanie crashlitics
- więcej...
